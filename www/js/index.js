var networkStatus = '<span style="color: red">OFFLINE</span>';
$(document).on('ready', function () {
    networkStatus = navigator.onLine ? '<span style="color: green">ONLINE</span>' : '<span style="color: red">OFFLINE</span>';
});

$(document).on('pagebeforeshow', '#networkpage', function () {
    $('#isOnline').html(networkStatus);
});

function ring() {
    navigator.notification.beep(3);
}

function vibrate() {
    navigator.vibrate(5000);
}

function refresh() {
    networkStatus = navigator.onLine ? '<span style="color: green">ONLINE</span>' : '<span style="color: red">OFFLINE</span>';
    $('#isOnline').html(networkStatus);
}